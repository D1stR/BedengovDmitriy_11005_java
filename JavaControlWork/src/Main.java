import java.io.*;

public class Main {

    public static void main(String[] args) {
        Storage storage = new Storage();
        try (BufferedReader reader = new BufferedReader(new FileReader("order.txt"))) {
            String str = reader.readLine();
            while (str  != null) {
                String[] line = str.split("\\|");
                storage.addOrder(line[0], line[1], line[2], Integer.parseInt(line[3]));
                str = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        storage.addOrder("Dmitriy","Kazan","Kola",15);
        storage.BuyerNameSort();
        storage.CountOfOrdersSort();
        storage.printAll();
    }
}

