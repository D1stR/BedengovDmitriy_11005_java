import java.io.*;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.ArrayList;

public class Storage {

    ArrayList<Buyer> buyers = new ArrayList<>();

    Buyer getBuyer(String name) {
        for (Buyer buyer : buyers) {
            if (buyer.getName().equals(name)) {
                return buyer;
            }
        }
        Buyer buyer = new Buyer(name);
        buyers.add(buyer);
        return buyer;
    }

    void addOrder(String name, String city, String order, int count) {
        Buyer buyer = getBuyer(name);
        buyer.addOrder(city, order, count);
    }
    @Override
    public String toString() {
        return buyers.toString();
    }

    public void BuyerNameSort(){
        BuyerByOrdersCountComparator orderCountComp = new BuyerByOrdersCountComparator();
        BuyerByCityCountComparator cityCountComp = new BuyerByCityCountComparator();
        BuyerByNameComparator nameComp = new BuyerByNameComparator();
        Comparator<Buyer> comparator = nameComp.thenComparing(cityCountComp).thenComparing(orderCountComp);
        TreeSet<Buyer> SortNames = new TreeSet<>(comparator);
        SortNames.addAll(buyers);
        try (FileWriter writer = new FileWriter("sortedByName.txt", true)) {
            for (Buyer buyer : SortNames){
                writer.write(buyer.toString());
                writer.write("\n");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CountOfOrdersSort(){
        BuyerByOrdersCountComparator orderCountComp = new BuyerByOrdersCountComparator();
        BuyerByCityCountComparator cityCountComp = new BuyerByCityCountComparator();
        BuyerByNameComparator nameComp = new BuyerByNameComparator();
        Comparator<Buyer> comparator = orderCountComp.thenComparing(cityCountComp).thenComparing(nameComp);
        TreeSet<Buyer> SortOrder = new TreeSet<>(comparator);
        SortOrder.addAll(buyers);
        try (FileWriter writer = new FileWriter("sortedByOrders.txt", true)) {
            for (Buyer buyer : SortOrder){
                writer.write(buyer.toString());
                writer.write("\n");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    void printAll(){
        for (int j = 0; j < buyers.size(); j++){
            System.out.println(buyers.get(j).getName() + "\n " );
            buyers.get(j).print();
        }
    }
}