public class Order {
    private String nameOfOrder;
    private int countOfOrder;

    public Order(String nameOfOrder, int countOfOrder) {
        this.nameOfOrder = nameOfOrder;
        this.countOfOrder = countOfOrder;
    }

    public String toString(){
        return nameOfOrder + " = " + countOfOrder;
    }
    public String getName() {
        return nameOfOrder;
    }

    public int getCount() {
        return countOfOrder;
    }

    public void setCount(int count) {
        this.countOfOrder = count;
    }
}