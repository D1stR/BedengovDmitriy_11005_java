import java.util.ArrayList;

public class City {
    private String nameOfCities;
    private ArrayList<Order> OrderList = new ArrayList<>();

    public City(String nameOfCities, Order OrderList) {
        this.nameOfCities = nameOfCities;
    }

    void addOrder(String nameOfCities, int count) {
        for (Order order : OrderList ){
            if (order.getName().equals(nameOfCities))
            { int i = order.getCount() + count;
                order.setCount(i);
                return;}
        }
        OrderList.add(new Order(nameOfCities, count));
    }

    public String toString(){
        return nameOfCities + OrderList;
    }

    public City(String nameOfCities) {
        this.nameOfCities = nameOfCities;
    }

    public String getName() {
        return nameOfCities;
    }

    public int getOrdersCount() {
        return OrderList.size();
    }

    void print(){
        System.out.println("   "+ nameOfCities + "\n" + "      " + OrderList.toString());
    }
}
