import java.util.Comparator;

class  BuyerByNameComparator  implements Comparator<Buyer> {

    @Override
    public int compare(Buyer buyer1, Buyer buyer2) {
        return buyer1.nameOfBuyer.compareTo(buyer2.nameOfBuyer);
    }
}

class BuyerByCityCountComparator implements  Comparator<Buyer> {

    @Override
    public int compare(Buyer buyer1, Buyer buyer2) {
        return Integer.compare(buyer1.CityList.size(),buyer2.CityList.size());
    }
}
class BuyerByOrdersCountComparator implements Comparator<Buyer>{

    @Override
    public int compare(Buyer buyer1, Buyer buyer2) {
        return Integer.compare(buyer1.getOrdersCount(),buyer2.getOrdersCount());
    }
}
