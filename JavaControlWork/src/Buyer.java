import java.util.ArrayList;

public class Buyer {
    String nameOfBuyer;
    ArrayList<City> CityList = new ArrayList<>();

    public Buyer(String nameOfBuyer) {
        this.nameOfBuyer= nameOfBuyer;
    }

    void addOrder(String city, String nameOfBuyer, int count) {
        for (City c : CityList) {
            if (c.getName().equals(city)) {
                c.addOrder(nameOfBuyer, count);
                return;
            }
        }
        CityList.add(new City(city));
        addOrder(city, nameOfBuyer, count);
    }

    public int getOrdersCount(){
        int count = 0;
        for(City c:CityList) {
            count += c.getOrdersCount();
        }
        return count;
    }

    public String toString(){
        return nameOfBuyer + CityList;
    }

    public String getName() {
        return nameOfBuyer;
    }

    public ArrayList<City> getCities() {
        return CityList;
    }

    void print(){
        for (int i = 0; i < CityList.size(); i++){
            CityList.get(i).print();
        }
    }
}