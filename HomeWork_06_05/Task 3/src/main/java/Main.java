public class Main {
    public static void main(String[] args) {
        ProductsFileSaver fileSaver = new ProductsFileSaver("products.txt");
        Sklad sklad = new Sklad(fileSaver);

        sklad.addListener(sklad::printProducts);
        sklad.addListener(() -> {
            int result = 0;
            for (Product p : sklad.products) result += p.getCount();
            System.out.println("Sklad size engaged: "+result);
        });
        sklad.addListener(() -> {
            int max = 0;
            String result = "";
            for (int i = 0; i < sklad.products.size(); i++) {
                if (sklad.products.get(i).getCount() > max) {
                    max = sklad.products.get(i).getCount();
                    result = sklad.products.get(i).name;
                }
            }
            System.out.println("Max: "+result);
        });


        sklad.createNewProduct("chair", 10);
        sklad.createNewProduct("table", 5);

        sklad.printProducts();
    }
}
