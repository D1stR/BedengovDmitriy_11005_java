import java.util.ArrayList;

public class Sklad {
    ProductsSaver saver;
    ArrayList<Product> products;
    ArrayList<ProductsListener> listeners = new ArrayList<>();
    public void addListener(ProductsListener listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }
    public Sklad(ProductsSaver saver) {
        this.saver = saver;
        products = saver.getProducts();
    }

    public void createNewProduct(String name, int count) {
        Product product = new Product(name, count);
        products.add(product);
        saver.saveProducts(products);
        for (ProductsListener listener : listeners) listener.productsUpdated();
    }

    public Product getProduct(String name) {
        for (Product p : products) {
            if (p.name.equals(name))
                return p;
        }

        return null;
    }

    public void deleteProduct(String name) {
        Product p = getProduct(name);
        if (p != null)
            products.remove(p);

        saver.saveProducts(products);
        for (ProductsListener listener : listeners) listener.productsUpdated();
    }

    public void printProducts() {
        for(Product p : products) {
            System.out.println(p.name + " in quantity " + p.count);
        }
    }
    public void changeProduct(String name, int count) {
        Product p = getProduct(name);

        if (p != null) {
            deleteProduct(name);
            createNewProduct(name, count);
        }
        for (ProductsListener listener : listeners) listener.productsUpdated();
    }
}
