
import java.util.*;
import java.util.function.Predicate;
public class Main {
    public static void main(String[] args) {
        TreeSet<String> treeSet = new TreeSet<>(Comparator.comparingInt(String::length));
        treeSet.add("table");
        treeSet.add("chair");
        treeSet.add("trigger");
        treeSet.add("set");
        treeSet.add("get");
        System.out.println(treeSet);

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(4);
        arrayList.add(3223);
        arrayList.add(-543);
        arrayList.add(523525235);
        ArrayList<Integer> evenNumbers = siftArray(arrayList, x ->  x % 2 == 0);
        ArrayList<Integer> greaterThanZero = siftArray(arrayList, x ->  x > 0);
        ArrayList<Integer> divisorOfFive = siftArray(arrayList, x ->  x % 5 == 0);
        System.out.println(evenNumbers);
        System.out.println(greaterThanZero);
        System.out.println(divisorOfFive);
    }
    public static ArrayList<Integer> siftArray(ArrayList<Integer> list, Predicate<Integer> predicate) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int x : list) {
            if (predicate.test(x))
                result.add(x);
        }
        return result;
    }
}
