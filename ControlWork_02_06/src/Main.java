import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


class Purchase{
    String name;
    String product;
    int count;

    public Purchase(String name, String product, int count) {
        this.name = name;
        this.product = product;
        this.count = count;
    }

    @Override
    public String toString() {
        return "Purchase" + " " + name +
                product + " " + count ;
    }
    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }

    public String getProduct() {
        return product;
    }
}
public class Main {
    public static void main(String[] args) {
        while (true){
            System.out.println("Type Task(1,2,3): ");
            Scanner in = new Scanner(System.in);
            int k = in.nextInt();
            switch (k){
                case 0:
                    return;
                case 1:
                    Thread t1 = new Thread(() -> {
                        try {
                            Task1();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    t1.start();
                    System.out.println("Поток 1 активен: " + t1.isAlive());
                    break;
                case 2:
                    Thread t2 = new Thread(() -> {
                        try {
                            Task2();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    t2.start();
                    System.out.println("Поток 2 активен: " + t2.isAlive());
                    break;
                case 3:
                    Thread t3 = new Thread(() -> {
                        try {
                            Task3();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    t3.start();
                    System.out.println("Поток 3 активен: " + t3.isAlive());
                    break;
            }
        }
    }

    public static void Task1 () throws Exception{
        BufferedReader br = new BufferedReader(new FileReader("task1.txt"));
        List<Purchase> listOfProducts = new ArrayList<>();
        String line;

        while ((line = br.readLine()) != null) {
            String[] splitLine = line.split("\\|");
            String name = splitLine[0];
            String order = splitLine[1];
            Integer amount = Integer.parseInt(splitLine[2]);
            listOfProducts.add(new Purchase(name, order, amount));
        }
        br.close();
        listOfProducts.stream()
                .collect(Collectors.groupingBy(Purchase::getName))
                .forEach((key, value) -> System.out.println(key + " " + value.stream().max(Comparator.comparingInt(Purchase::getCount)).get()));
    }

    public static void Task2()  throws Exception{
        FileReader fr = new FileReader("task2.txt");
        Scanner in = new Scanner(fr);
        String[] s = new String[10000];
        int i = 0;
        int sum = 0;
        while (in.hasNextLine()) {
            s[i] = in.nextLine();
            i++;
        }
        fr.close();

        Map<String, Integer> map = Arrays.stream(s).collect(Collectors.toMap(
                str1 -> str1.substring(0, str1.indexOf('|')),
                str1 -> Integer.parseInt(str1.substring(str1.indexOf('|') + 1)),
                (x, y) -> x = x + y));

        int k = map.size();
        for(Map.Entry<String, Integer> entry: map.entrySet()) {
            sum += entry.getValue();
        }
        sum = sum / k;
        for(Map.Entry<String, Integer> entry: map.entrySet()) {
            if (entry.getValue() > sum) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        }
    }
    public static void Task3() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader("task1.txt"));
        List<Purchase> listOfProducts = new ArrayList<>();
        String str;

        while ((str = br.readLine()) != null) {
            String[] splitLine = str.split("\\|");
            String name = splitLine[0];
            String product = splitLine[1];
            Integer count = Integer.parseInt(splitLine[2]);
            listOfProducts.add(new Purchase(name, product, count));
        }
        br.close();
        listOfProducts.stream()
                .collect(Collectors.groupingBy(Purchase::getProduct))
                .forEach((key, value) ->
                        System.out.println(key + ", min: " + value.stream().min(Comparator.comparingInt(Purchase::getCount)).get().getCount()
                                + " , max: " + value.stream().max(Comparator.comparingInt(Purchase::getCount)).get().getCount()));
    }
}