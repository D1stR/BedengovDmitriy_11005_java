import java.util.Scanner;
import java.util.*;
public class HashMapHomeWork {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        HashMap<String,Integer> map = new HashMap<>();
        System.out.println("Frequency Vocabulary ");
        System.out.println("Type string with one word one or more times: ");
        String str = in.nextLine();
        String[] value = str.toLowerCase().split(" ");
        Vocabulary(map,value);
        System.out.println("Shop");
        HashMap<String, HashMap<String,Integer>> purchase = new HashMap<>();
        int k = 1;
        while(k == 1){
            System.out.println("Type Username,Product Name,Number of purchase items");
            String purchaseStr = in.nextLine();
            String[] purchaseValue = purchaseStr.toLowerCase().split(" ");
            ShopPurchases(purchase, purchaseValue);
            System.out.println("Type 1 if u want to continue,type smth else if u want to stop");
            int enteredData = Integer.parseInt(in.nextLine());
            k = enteredData;
        }
    }
    static void Vocabulary(HashMap<String,Integer> map,String[] value){
        for (int i = 0; i < value.length;i++){
            if(map.containsKey(value[i])){
                map.put(value[i], map.get(value[i])+1);
            }else {
                map.put(value[i], 1);
            }
        }
        System.out.println(map);

    }
    static void ShopPurchases(HashMap<String, HashMap<String,Integer>> purchase,String[] purchaseValue){
        for (int i = 0; i < purchaseValue.length; i += 3) {
            HashMap<String,Integer> productMap = new HashMap<>();
            Integer z = Integer.parseInt(purchaseValue[i+2]);
            if (purchase.containsKey(purchaseValue[i])) {
                if (purchase.get(purchaseValue[i]).containsKey(purchaseValue[i+1])) {
                    productMap = new HashMap<>();
                    productMap.put((String) purchase.get(purchaseValue[i]).keySet().toArray()[0] ,(Integer) purchase.get(purchaseValue[i]).values().toArray()[0]);
                    productMap.put(purchaseValue[i+1],(Integer) (purchase.get(purchaseValue[i]).values().toArray()[0]) + z);
                    purchase.put(purchaseValue[i],productMap);
                } else {
                    productMap.put((String) purchase.get(purchaseValue[i]).keySet().toArray()[0], (Integer) purchase.get(purchaseValue[i]).values().toArray()[0]);
                    productMap.put(purchaseValue[i+1], z);
                    purchase.put(purchaseValue[i], productMap);
                }
            } else {
                productMap.put(purchaseValue[i+1], z);
                purchase.put(purchaseValue[i], productMap);
            }
        }
        System.out.println(purchase);
    }
}