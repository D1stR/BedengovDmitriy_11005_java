import java.util.Stack;
public class PolishCalculator {
    static boolean tryParseInt(String input) {
        try {
            Integer.parseInt(input);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    public static int arithmeticOperations(String str,int b, int a) {
        switch (str) {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return (a * b);
            case "/":
                return a / b;
            default:
                System.out.println("Error");
                return 0;
        }
    }
    public static void polishCalculator(String string){
        String[] str = string.split(" ");
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < str.length; i++) {
            if (tryParseInt(str[i])) {
                stack.push(Integer.parseInt(str[i]));
            } else {
                if (stack.size() >= 2) {
                    int b = stack.pop();
                    int a = stack.pop();
                    stack.push(arithmeticOperations(str[i],b,a));
                } else {
                    System.out.println("Error");
                }
            }
        }
        if (stack.size() > 1 || stack.size() == 0) {
            System.out.println("Error");
        } else{
            System.out.println(stack.pop());
        }

    }
    public static void main(String[] args) {
        String str1 = "2 3 +";
        String str2 = "2 3 * 4 5 * +";
        String str3 = "3 4 5 6 * + -";
        //String str4 = " 2 3 + * ";//error

        polishCalculator(str1);
        polishCalculator(str2);
        polishCalculator(str3);
        //polishCalculator(str4);

    }
}