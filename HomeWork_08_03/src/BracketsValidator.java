import java.util.Stack;

public class BracketsValidator {

    private Stack<Character> brackets = new Stack<Character>();

    private boolean isLeftBracket(char bracket) {
        return "({[".indexOf(bracket) != -1;
    }

    private boolean isRightBracket(char bracket) {
        return ")}]".indexOf(bracket) != -1;
    }

    private boolean isPair(char left, char right) {
        return left == '[' && right == ']' || left == '{' && right == '}'||left == '(' && right == ')';
    }

    public boolean validate(String input) {
        for (char c : input.toCharArray()) {
            if (isRightBracket(c) && brackets.isEmpty()) {
                return false;
            }
            if (isLeftBracket(c)) {
                brackets.push(c);
            }
            if (isRightBracket(c)) {
                if (isPair(brackets.peek(), c)) {
                    brackets.pop();
                } else {
                    return false;
                }
            }
        }
        return brackets.isEmpty();
    }
}