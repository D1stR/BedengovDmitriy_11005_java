public class BracketMain {

    public static void main(String[] args) {

        BracketsValidator validator = new BracketsValidator();


        String str4 = "[]dwd{wdw(s)}"; //правильная строка
        boolean answer4 = validator.validate(str4);

        String str3 ="sdw[dw{dfe}adaw]";//правильная строка
        boolean answer3 = validator.validate(str3);

        String str1 ="as[w(w)d+)";//строка с ошибкой, у открывающей скобки [ нет “пары”, то же про закрывающую )
        boolean answer1 = validator.validate(str1);

        String str2 ="asdw[dw(das]adw)";//строка с ошибкой, скобка [ открылась раньше и закрылась раньше
        boolean answer2 = validator.validate(str2);

        Answer(answer1);
        Answer(answer2);
        Answer(answer3);
        Answer(answer4);
    }
    public static void Answer(boolean str){
        if (str == true){
            System.out.println("Правильная строка");
        }else{
            System.out.println("Неправильная строка");
        }

    }
}