
import java.util.*;
public class Task1 {
    public static volatile long fact = 1;
    public static int n;
    public static void main(String[] args) throws InterruptedException {
        Scanner scan = new Scanner(System.in);

        System.out.println("Type number in the range from 0 to 20");
        n = scan.nextInt();
        if (n <= 5) {
            Thread1 t1 = new Thread1();
            t1.start();
            System.out.println("Поток 1 активен: " + t1.isAlive());
            t1.join();
        } else if (n <= 10) {
            Thread1 t1 = new Thread1();
            t1.start();
            System.out.println("Поток 1 активен: " + t1.isAlive());
            t1.join();
            Thread2 t2 = new Thread2();
            t2.start();
            System.out.println("Поток 2 активен: " + t2.isAlive());
            t2.join();
        } else if (n <= 15) {
            Thread1 t1 = new Thread1();
            t1.start();
            System.out.println("Поток 1 активен: " + t1.isAlive());
            t1.join();
            Thread2 t2 = new Thread2();
            t2.start();
            System.out.println("Поток 2 активен: " + t2.isAlive());
            t2.join();
            Thread3 t3 = new Thread3();
            t3.start();
            System.out.println("Поток 3 активен: " + t3.isAlive());
            t3.join();

        } else if (n <= 20) {
            Thread1 t1 = new Thread1();
            t1.start();
            System.out.println("Поток 1 активен: " + t1.isAlive());
            t1.join();
            Thread2 t2 = new Thread2();
            t2.start();
            System.out.println("Поток 2 активен: " + t2.isAlive());
            t2.join();
            Thread3 t3 = new Thread3();
            t3.start();
            System.out.println("Поток 3 активен: " + t3.isAlive());
            t3.join();
            Thread4 t4 = new Thread4();
            t4.start();
            System.out.println("Поток 4 активен: " + t4.isAlive());
            t4.join();
        }
        if (n < 0 || n > 20){
            System.out.println("Number is out of range");
            System.exit(0);
        }
        System.out.println("Answer: " + fact);

    }


    static class Thread1 extends Thread {
        @Override
        public void run() {
            if (n > 5) {
                for (int i = 1; i <= 5; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            } else {
                for (int i = 1; i <= n; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            }
            try {
                Thread1.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Thread2 extends Thread {
        @Override
        public void run() {
            if (n > 10) {
                for (int i = 6; i <= 10; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            } else {
                for (int i = 6; i <= n; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            }
            try {
                Thread2.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Thread3 extends Thread {
        @Override
        public void run() {
            if (n > 15) {
                for (int i = 11; i <= 15; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            } else {
                for (int i = 11; i <= n; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            }
            try {
                Thread2.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Thread4 extends Thread {
        @Override
        public void run() {
            if (n == 20) {
                for (int i = 16; i <= 20; i++) {
                    fact = fact * i;
                    System.out.println(i + " = " + fact);
                }
            } else {
                for (int i = 16; i <= n; i++) {
                    fact = fact * i;
                    System.out.println(+ i + " = " + fact);
                }
            }
            try {
                Thread2.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}