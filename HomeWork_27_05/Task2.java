import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Task2 {
    static volatile ArrayList evenCollection = new ArrayList();
    static volatile ArrayList oddCollection = new ArrayList();
    static ArrayList<Character> symbols = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        File file = new File("input.txt");
        Scanner scan1 = new Scanner(file);
        ArrayList<String> inputList = new ArrayList<>();
        while (scan1.hasNext()) {
            String line = scan1.next();
            inputList.add(line.toLowerCase(new Locale("ru")).replaceAll(" ", ""));
        }
        for (int i = 0; i < inputList.size(); i++) {
            String s = inputList.get(i);
            for (int j = 0; j < s.length(); j++) {
                if (!symbols.contains(s.charAt(j))) {
                    symbols.add(s.charAt(j));
                }
            }
            if (symbols.size() % 2 == 0) {
                Thread5 t5 = new Thread5();
                t5.start();
                System.out.println("Thread with even symbols: ");
                t5.join();
                symbols.removeAll(symbols);
            } else {
                Thread6 t6 = new Thread6();
                t6.start();
                System.out.println("Thread with odd symbols: ");
                t6.join();
                symbols.removeAll(symbols);
            }
        }
        System.out.println("Odd symbols string for words: " + oddCollection);
        System.out.println("Even symbols string for words: " + evenCollection);
    }

    static class Thread5 extends Thread {
        @Override
        public void run() {
            add(symbols);
            try {
                Thread5.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public static void add(ArrayList<Character> list) {
            String evenWord = list.toString();
            evenCollection.add(evenWord);
            System.out.println(evenWord);

        }
    }
    static class Thread6 extends Thread {
        @Override
        public void run() {
            add(symbols);
            try {
                Thread6.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public static void add(ArrayList<Character> list) {
            String oddWord = list.toString();
            oddCollection.add(oddWord);
            System.out.println(oddWord);

        }
    }
}