import org.junit.*;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;
public class CrudTest {
    private Main.CRUDHolder<Employee> employee;
    private ArrayList<Employee> a;
    @Before
    public void prepareEmployees() {
        a = new ArrayList<>();
        employee = new Main.CRUDHolder<>(a);
        a.add(new Employee("Dmitriy",500,1));
        a.add(new Employee("Alex", 2000,2));
        a.add(new Employee("Andrey",10000,3));
    }
    @Test
    public void createTestSucceed() {
        employee.create(new Employee("Ricardo", 50,4));

        assertEquals("Ricardo", a.get(3).getName());
        assertEquals(50, a.get(3).getSalary());
        assertEquals(4, a.get(3).getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createTestFailed() {
        employee.create(new Employee("Alex",2000 ,2));
    }

    @Test
    public void readTestSucceed() {
        Employee emp = employee.read(2);
        assertEquals(emp, a.get(2));
    }

    @Test(expected = NoSuchElementException.class)
    public void readTestFailed() {
        Employee emp = employee.read(4);
    }

    @Test
    public void updateTestSucceed() {
        employee.update(new Employee("Dmitriy", 5000,15));

        assertEquals("Dmitriy", a.get(0).getName());
        assertEquals(5000, a.get(0).getSalary());
        assertEquals(15, a.get(0).getId());
    }

    @Test(expected = NoSuchElementException.class)
    public void updateTestFailed() {
        Employee emp = new Employee("Vasiliy",1,12);
        employee.update(emp);
    }

    @Test
    public void deleteTestSucceed() {
        employee.delete(new Employee("Dmitriy", 500,1));

        assertEquals(2, a.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void deleteTestFailed() {
        employee.delete(new Employee("Kazantip", 5,32));
    }
}
