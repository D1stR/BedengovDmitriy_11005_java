package Stack;

public class Stack<T> {

    static class Node<T> {
        private T item;
        private  Node<T> next;
    }

    private Node<T> head;

    public Stack() {
    }
    public Boolean isEmpty() {
        return head == null;
    }

    public void push(T item){
        Node<T> node = new Node<>();
        node.item = item;
        node.next = head;
        head = node;
    }

    public T pop(){
        if (isEmpty()) throw new NullPointerException("Стэк пуст");
        T item = head.item;
        head = head.next;
        return item;

    }

    public T peek(){
        if (isEmpty()) throw new NullPointerException("Стэк пуст");
        return head.item;
    }
}