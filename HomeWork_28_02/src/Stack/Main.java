package Stack;

public class Main {
    public static void main(String[] args) throws Exception {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(42);
        stack.push(33232);
        stack.push(68);
        int a = stack.pop(); //delete
        int b = stack.peek(); //check
        stack.pop(); // удаляем из стека 68
        int c = stack.pop(); // получается 42
        System.out.println(b);
        //stack.pop();  - если добавить это,то выведет,что стэк пуст.
        System.out.println(a);
        System.out.println(c);

    }
}