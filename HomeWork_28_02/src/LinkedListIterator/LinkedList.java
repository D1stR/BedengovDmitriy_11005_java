package LinkedListIterator;
import java.util.Iterator;

public class LinkedList<T> implements Iterable<T> {

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    static class Node<T> {
        private T item;
        private Node<T> next;
    }

    private Node<T> head;

    public class ListIterator implements Iterator<T>{
        Node<T> iteratorHead = head;

        @Override
        public boolean hasNext() {
            return iteratorHead != null;
        }

        @Override
        public T next() {
            T item = iteratorHead.item;
            iteratorHead = iteratorHead.next;
            return  item;
        }
    }

    public LinkedList(){

    }
    public Boolean isEmpty() {
        return head == null;
    }

    public void add(T item){
        Node<T> node = new Node<>();
        node.item = item;
        if (head == null){
            head = node;
        }
        else{
            Node<T> temp = head;
            while (temp.next != null){
                temp = temp.next;
            }
            temp.next = node;
        }
    }



    public T get(int i) throws Exception {
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Лист пуст");
        }
        else {
            Node<T> temp = head;
            while (k != i) {
                if (temp.next != null) {
                    temp = temp.next;
                    k++;
                } else throw new Exception("Неверное значение");
            }
            return temp.item;
        }
    }
    public int count() throws Exception {
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Лист пуст");
        } else {
            Node<T> temp = head;
            while (temp.next != null) {
                temp = temp.next;
                k++;
            }
            return k+1;
        }
    }

    public void remove(int i) throws Exception{
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Лист пуст");
        } else {
            if (i == 0){
                head = head.next;
            } else {
                Node<T> prev = head;
                while (k != i - 1){
                    if (prev.next != null) {
                        prev = prev.next;
                        k++;
                    } else {throw new Exception("Неверное значение");}
                }
                if (prev.next != null) {
                    Node<T> temp = prev.next;
                    prev.next = temp.next;
                } else {throw new Exception("Неверное значение");}
            }
        }
    }
}
