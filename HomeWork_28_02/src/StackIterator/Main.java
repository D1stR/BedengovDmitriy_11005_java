package StackIterator;

public class Main {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(424);
        stack.push(343);
        stack.push(53523);
        stack.push(4633);
        stack.push(2);
        for (Integer item : stack) {
            System.out.println(item);
        }
    }
}
