package StackIterator;
import java.util.Iterator;

public class Stack<T> implements Iterable<T> {
    @Override
    public Iterator<T> iterator() {
        return new StackIterator();
    }

    static class Node<T> {
        private T item;
        private  Node<T> next;
    }

    private Node<T> head;

    public class StackIterator implements Iterator<T> {
        Node<T> iteratorHead = head;
        @Override
        public boolean hasNext() {
            return iteratorHead != null;
        }
        @Override
        public T next() {
            T temp = iteratorHead.item;
            iteratorHead = iteratorHead.next;
            return temp;
        }
    }

    public Stack() {
    }

    public Boolean isEmpty() {
        return head == null;
    }
    public void push(T item){
        Node<T> newNode = new Node<>();
        newNode.item = item;
        newNode.next = head;
        head = newNode;
    }

    public T pop(){
        if (isEmpty()) throw new NullPointerException("Стэк пуст");
        T item = head.item;
        head = head.next;
        return item;

    }

    public T peek(){
        if (isEmpty()) throw new NullPointerException("Стэк пуст");
        return head.item;
    }
}
