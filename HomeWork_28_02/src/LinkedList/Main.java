package LinkedList;

public class Main {
    public static void main(String[] args) throws Exception {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(3005);
        list.add(222);
        list.add(571);
        list.add(34);
        System.out.println(list.count());//считает кол-во элементов
        System.out.println(list.get(0));//выводит 1ый,введеный элемент
        list.remove(0);// убирает 1ый элемент,т.е. 2ой элемент становится 1ым
        System.out.println(list.get(0)); //выводит 1ый элемент,после изменений
        System.out.println(list.count());//считает кол-во элементов
        list.remove(0); //опять убирает
        list.remove(0);
        System.out.println(list.get(0));
    }
}
