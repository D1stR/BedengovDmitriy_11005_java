package LinkedList;

public class LinkedList<T> {
    static class Node<T> {
        private T item;
        private Node<T> next;
    }

    private Node<T> head;

    public LinkedList(){

    }
    public Boolean isEmpty() {
        return head == null;
    }

    public void add(T item){
        Node<T> node = new Node<>();
        node.item = item;
        if (head == null){
            head = node;
        }
        else{
            Node<T> temp = head;
            while (temp.next != null){
                temp = temp.next;
            }
            temp.next = node;
        }
    }



    public T get(int i) throws Exception {
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Нет элементов");
        }
        else {
            Node<T> temp = head;
            while (k != i) {
                if (temp.next != null) {
                    temp = temp.next;
                    k++;
                } else throw new Exception("Неверный номер");
            }
            return temp.item;
        }
    }
    public int count() throws Exception {
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Нет элементов");
        } else {
            Node<T> temp = head;
            while (temp.next != null) {
                temp = temp.next;
                k++;
            }
            return k+1;
        }
    }

    public void remove(int i) throws Exception{
        int k = 0;
        if (isEmpty()) {
            throw new Exception("Нет элементов");
        } else {
            if (i == 0){
                head = head.next;
            } else {
                Node<T> prev = head;
                while (k != i - 1){
                    if (prev.next != null) {
                        prev = prev.next;
                        k++;
                    } else {throw new Exception("Неверный номер");}
                }
                if (prev.next != null) {
                    Node<T> temp = prev.next;
                    prev.next = temp.next;
                } else {throw new Exception("Неверный номер");}
            }
        }
    }
}
