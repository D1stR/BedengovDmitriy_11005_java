import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class SignUpService {
    private final PasswordBlackList passwordBlackList;

    public SignUpService(PasswordBlackList passwordBlackList) {
        this.passwordBlackList = passwordBlackList;
    }

    public void signUp(String password) {
        if (passwordBlackList.contains(password)) {
            System.err.println("Пароль не подходит!");
        } else {
            System.out.println("Пароль хороший");
        }
    }
}
