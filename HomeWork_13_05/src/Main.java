import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {

        System.out.println("Task 1: ");
        String[] array = {"a1","a2","a3","b3","sadadada","Array","arrays","a4"};
        Stream<String> stream = Arrays.stream(array);

        stream.filter(x -> x.startsWith("a"))
                .sorted()
                .forEach(System.out::println);

        System.out.println("\nTask 2: ");
        Integer[] integers = {123,321,321,213,412};
        Stream<Integer> integerStream = Arrays.stream(integers);
        integerStream.sorted(Comparator.comparingInt(x -> x % 10))
                .map(x -> x / 10)
                .distinct()
                .forEach(System.out::println);

        System.out.println("\nTask 3: ");
        Map<String, Integer> map = new HashMap<>();

        Stream<String> stream1 = Files.lines(Paths.get("task3.txt"));
        stream1.map(x -> x.split("\\|"))
                .collect(Collectors.groupingBy(x -> x[0]))
                .forEach((key, value) -> map.put(
                        key,
                        value.stream()
                                .reduce(0,
                                        (y, z) -> y + Integer.parseInt(z[1]),
                                        Integer::sum)));
        map.forEach((x, y) -> System.out.println("Product: "+ x + "  Quantity:  " + y));
    }
}
