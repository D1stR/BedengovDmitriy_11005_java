package HomeWork_21_02;
import java.util.Iterator;

public class FibonacciRange implements Iterable<Integer> {
    protected int length;
    protected int count;
    private int firstNumber = 0;
    private int secondNumber = 1;
    private int s;

    public FibonacciRange(int length) {
        this.length = length;
        this.count = 0;
    }

    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return count < length;
            }

            @Override
            public Integer next() {
                switch (count) {
                    case 0:
                        count++;
                        return firstNumber;
                    case 1:
                        count++;
                        return secondNumber;
                    default:
                        count++;
                        s = firstNumber + secondNumber;
                        firstNumber = secondNumber;
                        secondNumber = s;
                        return s;
                }
            }
        };
    }
}
