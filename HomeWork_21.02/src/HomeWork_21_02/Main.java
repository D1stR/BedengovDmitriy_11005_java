package HomeWork_21_02;
public class Main {
    public static void main(String[] args) {
        FourTuple<Integer,Integer,String,String> tuples = new FourTuple<>(22, 532, "Home", "Work");

        System.out.println("1st Task");
        System.out.println(tuples.first);
        System.out.println(tuples.second);
        System.out.println(tuples.third);
        System.out.println(tuples.four);


        System.out.println("2nd task");
        int[] a = new int[] {4,5,2,3,5,2};
        for (int i : new RangeInterval(3, a.length)) {
            a[i] = 7;
        }
        for (int i : new Range(a.length,0)) {
            System.out.println(a[i]);
        }

        System.out.println("3rd Task");
        for (int i : new FibonacciRange(5)) {
            System.out.println(i);
        }
    }
}
