import java.io.Serializable;

public class Product implements Serializable {
    protected String name;
    protected Integer count;

    public Product(String name, Integer count){
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    @Override
    public String toString() {
        return "Product with name  " + name + " have " + count + " units of product";
    }
}
