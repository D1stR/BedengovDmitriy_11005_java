import java.io.*;
import com.google.gson.Gson;
import java.util.Scanner;
import java.util.ArrayList;


public class Main   {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        new FileWriter("text.txt", false).close();
        ArrayList<Product> product = new ArrayList<>();
        StorageOfGoods order = new StorageOfGoods();
        boolean flag = true;
        while (flag) {
            System.out.println(" Type 1 to add \n Type 2 to remove \n Type 3 to Write file \n Type 4 to Read file \n Type 5 to change count of product \n Type 6 to see all products \n Type 7 to end");
            char key = in.next().charAt(0);
            switch (key) {

                case '1': {
                    order.addProduct(product);
                    break;
                }
                case '2': {
                    product.remove(order.removeProduct(product));
                    break;
                }
                case '3': {
                    order.writeFile(product);
                    break;
                }
                case '4': {
                    order.readFile();
                    break;
                }
                case '5': {
                    System.out.println("Enter a name of change product and count of it: ");
                    String name = in.next();
                    int count = in.nextInt();
                    product.get(order.changeCountOfProduct(product, name)).count = count;
                    break;
                }
                case '6': {
                    order.printAll(product);
                    break;
                }
                case '7': {
                    flag = false;
                    break;
                }
            }
        }

    }
}

