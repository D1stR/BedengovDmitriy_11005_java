import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.awt.image.AreaAveragingScaleFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.lang.reflect.Type;
import java.util.Scanner;
public class StorageOfGoods {
    public void writeFile(ArrayList<Product> product) {
        Gson gson = new Gson();
        try {
            FileWriter fileWriter = new FileWriter("text.txt");
            gson.toJson(product, fileWriter);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Product> readFile() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Product>>() {}.getType();
        ArrayList<Product> orders = null;
        try {
            FileReader fileReader = new FileReader("text.txt");
            orders = gson.fromJson(fileReader, type);
            fileReader.close();
            System.out.println(orders);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orders;
    }

    public void printAll(ArrayList<Product> orders) {
        for (int i = 0; i < orders.size(); i++) {
            System.out.println(orders.get(i).toString());
        }
    }

    public void addProduct(ArrayList<Product> orders) {
        Scanner in = new Scanner(System.in);
        System.out.println("Type count of product to add");
        int count = in.nextInt();
        for (int i = 0; i < count; i++) {
            System.out.println("Type name and count of product ");
            orders.add(new Product(in.next(), in.nextInt()));
        }
    }

    public int removeProduct(ArrayList<Product> orders) {
        Scanner in = new Scanner(System.in);
        int с = 0;
        System.out.println("Type the product you want to remove ");
        String name = in.next();
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).equals(name)) {
                с = i;
            }
        }
        return с;
    }


    public int changeCountOfProduct(ArrayList<Product> orders , String nameOfChangeProduct) {
        int с = 0;
        System.out.println("Type product and after count u want to change ");
        for (int i = 0; i < orders.size(); i++) {
            if (orders.get(i).equals(nameOfChangeProduct)) {
                с = i;
            }
        }
        return с;
    }
}
