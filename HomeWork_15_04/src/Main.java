import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        Bot bot = new Bot();
        System.out.println("Введите команду,которые доступны:\n" + "Введите help,чтобы узнать все команды\n" +
                "Не забудьте ввести exit,когда закончите работать!:)");
        while (true) {
            System.out.print("Вводите: ");
            String message = in.nextLine();
            if (message.equals("exit")) {
                System.out.println("Спасибо,что пользуете нашим ботом!:)");
                break;
            }
            String answer = bot.processUserInput(message);
            System.out.println(answer);
        }
    }
}