import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Commands {
    String name();
    String description() default " ";
    String[] aliases() default " ";
    int id();
    boolean inProgress() default true;
}