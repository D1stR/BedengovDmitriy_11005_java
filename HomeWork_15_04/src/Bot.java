import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

public class Bot {
    private static final HashMap<String, Method> commands = new HashMap<>();
    private String[] splitStr;

    public Bot() {
        for(Method m : this.getClass().getDeclaredMethods()){
            if(!m.isAnnotationPresent(Commands.class)){
                continue;
            }
            Commands cmd = m.getAnnotation(Commands.class);
            if(cmd.inProgress()){
                continue;
            }
            commands.put(cmd.name().toLowerCase(), m);
            for(String name : cmd.aliases()){
                commands.put(name.toLowerCase(), m);
            }
        }
    }

    public String processUserInput(String input){
        if (input.isEmpty())
            return "Я вас не понимаю";

        String[] args = new String[]{};

        String command = input.toLowerCase();
        if(commands.get(command) == null){
                splitStr = command.split(" ");
            command = splitStr[0].toLowerCase();
            args = Arrays.copyOfRange(splitStr, 1, splitStr.length);
        }

        Method m = commands.get(command);

        try {
            String answer = (String) m.invoke(this, (Object) args);
            if(m.isAnnotationPresent(Commands.class)){
                Commands cmd = m.getAnnotation(Commands.class);
                String name = cmd.name().toLowerCase();
            }
            return answer;
        } catch (Exception e) {
            return "-> Что-то пошло не так, попробуйте ещё раз";
        }

    }

    @Commands(id = 1, inProgress = false, name = "Сумма", aliases = {"amount", "sum", "сложи"}, description = "Например: sum 5 10")
    public String sum(String[] args){
        int sum = Integer.valueOf(splitStr[1]) + Integer.valueOf(splitStr[2]);
        String result = "Ответ: " + sum;
        return result;
    }

    @Commands(id = 2, inProgress = false, name = "Разность", aliases = {"difference", "diff", "вычти"}, description = "Например: diff 22 7")
    public String difference(String[] args){
        int diff = Integer.valueOf(splitStr[1]) - Integer.valueOf(splitStr[2]);
        String result = "Ответ: " + diff;
        return result;
    }

    @Commands(id = 3, inProgress = false, name = "Умножение", aliases = {"multiplication", "multply", "умножь"}, description = "Например: multiply 5 5")
    public String multiplication(String[] args){
        int multply = Integer.valueOf(splitStr[1]) * Integer.valueOf(splitStr[2]);
        String result = "Ответ: " + multply;
        return result;
    }

    @Commands(id = 4, inProgress = false, name = "Деление", aliases = {"division", "div", "раздели"}, description = "Например: div 40 5")
    public String division(String[] args){
        float div = Float.valueOf(splitStr[1]) / Integer.valueOf(splitStr[2]);
        String result = "Ответ: " + div;
        return result;
    }

    @Commands(id = 5, inProgress = false, name = "Возведение в степень", aliases = {"degree", "deg", "возведи"}, description = "Например: deg 5 2")
    public String degree(String[] args){
        int num = Integer.valueOf(splitStr[1]);
        int degree = Integer.valueOf(splitStr[2]);
        int result = 1;
        while (degree > 0) {
            result *= num;
            degree--;
        }
        String results = "Ответ: " + result;
        return results;
    }

    @Commands(id = 6, inProgress = false, name = "Факториал", aliases = {"factorial", "fact", "факториал"}, description = "Например: fact 5")
    public String fact(String[] args){
        int fact = Integer.valueOf(splitStr[1]);
        long factorial = 1;
        while (fact > 0) {
            factorial *= fact;
            fact--;
        }
        String result = "Ответ: " + factorial;
        return result;
    }

    @Commands(id = 7, inProgress = false, name = "Считаем логарифм", aliases = {"log", "logarithm", "логарифм"}, description = "Например: log 8 2")
    public String log(String[] args){
        double a = Double.valueOf(splitStr[1]);
        double b = Double.valueOf(splitStr[2]);
        double result = Math.log(a) / Math.log(b);
        String results = "Ответ: " + result;
        return results;
    }

    @Commands(id = 8, inProgress = false, name = "Извлечение корня", aliases = {"root", "rooting", "извлечи"}, description = "Например: root 8")
    public String root(String[] args){
        double a = Double.valueOf(splitStr[1]);
        double result = Math.sqrt(a);
        String res = "Ответ: " + result;
        return res;
    }

    @Commands(id = 9, inProgress = false, name = "Команды", aliases = {"help", "aliases", "помощь"})
    public String help(String[] args) {
        StringBuilder builder = new StringBuilder("Доступные команды:\n");

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Commands.class)) {
                continue;
            }
            Commands cmd = m.getAnnotation(Commands.class);
            builder.append(cmd.name()).append(": ").append(Arrays.toString(cmd.aliases())).append(cmd.description()).append("\n");
        }

        return builder.toString();
    }
}